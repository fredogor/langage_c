#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "binome2degre.h"

double calculDeterminant(double _a, double _b, double _c)
{
    double _delta = 0;
    _delta = _b*_b - 4.0*_a*_c;

    return _delta;
}

double calculRacine1(double _a, double _b, double _delta)
{
    double _x1 = 0;
    _x1 = (-_b + sqrt(_delta))/(2.0 * _a);

    return _x1;
}

double calculRacine2(double _a, double _b, double _delta)
{
    double _x2 = 0;
    _x2 = (-_b - sqrt(_delta))/(2.0 * _a);

    return _x2;
}
