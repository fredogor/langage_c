#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "binome2degre.h"

int main(int argc, char *argv[])
{
    double a = 0, b = 0, c = 0;
    double delta = 0;
    double x1 = 0, x2 = 0;

    printf("\n--- Calcul des racines du binôme du 2° degré ---\n");
    printf("donner les coefficients du binôme :\n");

    printf("a = ");
    scanf("%lf", &a);
    printf("b = ");
    scanf("%lf", &b);
    printf("c = ");
    scanf("%lf", &c);

    printf("vous avez rentré : a = %lf\n",a);
    printf("vous avez rentré : b = %lf\n",b);
    printf("vous avez rentré : c = %lf\n",c);
    printf("-----------------------------------------------\n");
    delta = calculDeterminant(a, b, c);
    printf("calcul du déterminant : delta = %lf\n",delta);
    printf("-----------------------------------------------\n");

    if (delta > 0) {
        printf("delta > 0 : 2 racines :\n");
        x1 = calculRacine1(a, b, delta);
        x2 = calculRacine2(a, b, delta);
        printf("x1 = %lf\n", x1);
        printf("x2 = %lf\n", x2);
    }
    else if (delta < 0) printf ("delta < 0 : pas de racines !\n");
    else {
        printf("delta = 0 : 1 racine double !\n");
        x1 = calculRacine1(a, b, delta);
        x2 = calculRacine2(a, b, delta);
        printf("x1 = x2 = %lf\n", x1);
    }

    printf("-----------------------------------------------\n\n");
    return 0;
}
