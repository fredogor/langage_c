// lib pour calcul des racines du 2° degré

// le determinant
double calculDeterminant(double a, double b, double c);

// la 1° racine
double calculRacine1(double a, double b, double delta);

// la 2° racine
double calculRacine2(double a, double b, double delta);
